#include <ESP32CAN.h>
#include <CAN_config.h>
#include <string.h>

#define ACCELEROMETER_X_ADDR 0x11
#define ACCELEROMETER_Y_ADDR 0x12
#define ACCELEROMETER_Z_ADDR 0x13

#define TEMP_SENSOR_ADDR 0x21

#define THIRD_SENSOR_ADDR 0x31


#define WIFI_ENABLED 0
#define BUFFOR_LEN 100
#if WIFI_ENABLED
  #include <WiFi.h>
  WiFiClient client;
  WiFiUDP udpClient;
#endif
CAN_device_t CAN_cfg;


const char* ssid = "RedmiPro";//"TP-Link_2G";
const char* password = "Radek$1234";//"SebaCwel69";
const uint16_t port = 1313;
const char* host = /*"192.168.0.3";*/"192.168.43.239";

void printU64(uint64_t toPrint){
  int low = toPrint & 0xffffffff;
  int high = toPrint >> 32;
  Serial.print(high,HEX); Serial.print(low, HEX);Serial.print(" ");
}

uint64_t u8arrayTou64(uint8_t* src){
  uint64_t temp = 0;
  for (int i=0; i < 8; i++){
    temp <<= 8;
    temp |= (uint64_t)src[i];
  }
  return temp;
}

float conv_uint_to_float(uint64_t number){
    float temp = ((float) number)/1000000; //1e6
    return temp;
}

void setup() {
    Serial.begin(115200);
#if WIFI_ENABLED
    WiFi.begin(ssid, password);
    while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.println("...");
    }
    Serial.print("Connected with ip: "); Serial.println(WiFi.localIP());
    
#endif
    Serial.println("iotsharing.com CAN demo");
    CAN_cfg.speed=CAN_SPEED_1000KBPS;
    //CAN_cfg.tx_pin_id = GPIO_NUM_5;
    //CAN_cfg.rx_pin_id = GPIO_NUM_4;
    CAN_cfg.tx_pin_id = GPIO_NUM_13;
    CAN_cfg.rx_pin_id = GPIO_NUM_12;
    CAN_cfg.rx_queue = xQueueCreate(10,sizeof(CAN_frame_t));
    //initialize CAN Module
    ESP32Can.CANInit();
}
//uint8_t dataBuffer [BUFFOR_LEN][12];
uint32_t rcvAddress;
uint8_t rcvData[8] = {0x11, 0x99, 0xaa, 0xbb, 0xcc, 0xdd, 0xee, 0xff};
String buffer = "";
int iterator = 0;
void loop() {
    CAN_frame_t rx_frame;
    //receive next CAN frame from queue
    
    
    if(xQueueReceive(CAN_cfg.rx_queue,&rx_frame, 3*portTICK_PERIOD_MS)==pdTRUE){
      //printf("New frame with id: %x\n", rx_frame.MsgID);
      if(rx_frame.FIR.B.FF==CAN_frame_std)
       ;// printf("This is standard frame!\n");
      else
        ;//printf("This is extended frame!\n");

      if(rx_frame.FIR.B.RTR==CAN_RTR)
        ;//printf(" RTR from 0x%08x, DLC %d\r\n",rx_frame.MsgID,  rx_frame.FIR.B.DLC);
      else{
        //printf(" from 0x%08x, DLC %d\n",rx_frame.MsgID,  rx_frame.FIR.B.DLC);
        rcvAddress = rx_frame.MsgID;
        //Serial.print("Push msgID: "); printU64(rcvAddress);

        /* Tutaj ma być CLI */
        switch (rcvAddress){
          case TEMP_SENSOR_ADDR:{
            Serial.print("Received temperature! ");
            for(uint8_t i = 0; i < 8; i++) rcvData[i]= rx_frame.data.u8[i];
            float result = conv_uint_to_float(u8arrayTou64(rcvData));
            Serial.print("Current value is... "); Serial.println(result);
          }break;
          /* itd...*/
        }

      }
      
      if(iterator == BUFFOR_LEN)
      {

        #if WIFI_ENABLED 
          udpClient.beginPacket(host,port);
          for(int i = 0; i < BUFFOR_LEN * 12; i++) udpClient.write(tempTable[i]);
          udpClient.endPacket();
        #endif
        iterator = 0;
        //client.flush();

      }
      //respond to sender
      //ESP32Can.CANWriteFrame(&rx_frame);
    }
}
