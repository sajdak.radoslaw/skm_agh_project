#include <ESP32CAN.h>
#include <CAN_config.h>
#include <Wire.h>
#include <OneWire.h>
#include <DallasTemperature.h>
#include <Adafruit_MPU6050.h>

#define ACCELEROMETER 0
#define TEMP_SENSOR 1
#define DISTANCE_SENSOR 0
#define CAN 1

#if ACCELEROMETER | TEMP_SENSOR |THIRD_SENSOR
  #define ACCELEROMETER_X_ADDR 0x11
  #define ACCELEROMETER_Y_ADDR 0x12
  #define ACCELEROMETER_Z_ADDR 0x13

  #define TEMP_SENSOR_ADDR 0x21

  #define THIRD_SENSOR_ADDR 0x31
#endif

#define TRIG_PIN 2
#define ECHO_PIN 5

#define CAN_TX 5
#define CAN_RX 4 // rx=>rx


Adafruit_MPU6050 accelerometer;

const int oneWireBus = GPIO_NUM_15;
OneWire oneWire(oneWireBus);
DallasTemperature sensors (&oneWire);

float temperatureSensor(){
  float tempCelcius;  
  sensors.requestTemperatures();
  tempCelcius = sensors.getTempCByIndex(0);
  /*....*/
  return tempCelcius;
}

void accelerometerSensor(float* destination){
    sensors_event_t acc, gyro, temp;
    accelerometer.getEvent(&acc, &gyro, &temp);
    /* Conversion reasult to uint64_t */

    destination[0] = acc.acceleration.x;
    destination[1] = acc.acceleration.y;
    destination[2] = acc.acceleration.z;
    delay(50);
}

float distanceSensor(){
  float czas, dystans;
 
  digitalWrite(TRIG_PIN, LOW);
  delayMicroseconds(2);
  digitalWrite(TRIG_PIN, HIGH);
  delayMicroseconds(10);
  digitalWrite(TRIG_PIN, LOW);
 
  czas = pulseIn(ECHO_PIN, HIGH);
  dystans = czas * 0.034/2;
  /* Conversion from float to int */
  return dystans;
}
CAN_device_t CAN_cfg;
void printU64(uint64_t toPrint){
  int low = toPrint & 0xffffffff;
  int high = toPrint >> 32;
  Serial.print(high,HEX); Serial.print(low, HEX);Serial.print(" ");
}
void emulateDevice(uint64_t data, uint32_t address, uint8_t DLC)
{
  CAN_frame_t tx_frame;
  uint32_t data_h = data >> 32;
  uint32_t data_l = data & 0xffffffff;
  tx_frame.FIR.B.FF = CAN_frame_std;
  tx_frame.MsgID = address;
  tx_frame.FIR.B.DLC = DLC;
  // for(uint8_t i = 0; i < DLC; i++){
  //   if(i < 4) tx_frame.data.u8[i] = (data_h & (0xff << (24 - 8*i))) >> (24 - 8*i);
  //   else tx_frame.data.u8[i] = (data_l & (0xff << (24 - 8*(i - 4)))) >> (24 - 8*(i-4));
  // }
  for(uint8_t i = 0; i < 8; i++){
    tx_frame.data.u8[7 - i] = data & 0xff;
    data >>= 8;
  }
  ESP32Can.CANWriteFrame(&tx_frame);
  Serial.print("MSG_ID "); Serial.println(tx_frame.MsgID, HEX);
  delay(50); //We need to change it for sth like wait for ACK.
}

/* conversion funcs */
uint64_t conv_float_to_uint(float number){
    uint64_t temp = (uint64_t) (number*1000000); //1e6
    return temp;
}

float conv_uint_to_float(uint64_t number){
    float temp = ((float) number)/1000000; //1e6
    return temp;
}

void setup() {
  Serial.begin(115200);
  Serial.println("iotsharing.com CAN demo");
  if ( DISTANCE_SENSOR ){
    pinMode(TRIG_PIN, OUTPUT);
    pinMode(ECHO_PIN, INPUT);
  }
  if( CAN ){
    CAN_cfg.speed=CAN_SPEED_1000KBPS;
    //CAN_cfg.tx_pin_id = GPIO_NUM_13;  //ESP32-CAM
    //CAN_cfg.rx_pin_id = GPIO_NUM_12;
    CAN_cfg.tx_pin_id = GPIO_NUM_5; // ESP32 DEVkit
    CAN_cfg.rx_pin_id = GPIO_NUM_4;
    CAN_cfg.rx_queue = xQueueCreate(100,sizeof(CAN_frame_t));
    //start CAN Module
    ESP32Can.CANInit();
  } // CAN
  if (ACCELEROMETER){
    accelerometer.begin();
    accelerometer.setAccelerometerRange(MPU6050_RANGE_8_G);
    accelerometer.setGyroRange(MPU6050_RANGE_500_DEG);
    accelerometer.setFilterBandwidth(MPU6050_BAND_5_HZ);
  }
  if (TEMP_SENSOR){
    sensors.begin();  
  }
} 

int iterator = 0;
uint64_t var = 0x112321;
uint32_t address_can = 0x0000;

/* Sensors */
float xyz[3];
float temperature;
float distance;
void loop() {
  
  if (DISTANCE_SENSOR){
    distance = distanceSensor();
    //Serial.print("Distance : "); Serial.println(distance);
    delay(500);
  }
  if(ACCELEROMETER){
    accelerometerSensor(xyz);
    for(int i = 0; i < 3; i ++)
    {
      Serial.print(xyz[i]); Serial.print("  ");
    }
    Serial.println();
  }  

  if( CAN ){
    CAN_frame_t rx_frame;
    //receive next CAN frame from queue
    if(xQueueReceive(CAN_cfg.rx_queue,&rx_frame, 3*portTICK_PERIOD_MS)==pdTRUE){

      //do stuff!
      if(rx_frame.FIR.B.FF==CAN_frame_std)
       ;// printf("New standard frame");
      else
      ;//  printf("New extended frame");

      if(rx_frame.FIR.B.RTR==CAN_RTR)
        ;//printf(" RTR from 0x%08x, DLC %d\r\n",rx_frame.MsgID,  rx_frame.FIR.B.DLC);
      else{
        ;//printf(" from 0x%08x, DLC %d\n",rx_frame.MsgID,  rx_frame.FIR.B.DLC);
        // for(int i = 0; i < 8; i++){
        //   printf("%c\t", (char)rx_frame.data.u8[i]);
        // }
        // printf("\n");
      }
    }
    else
    {
      if( ACCELEROMETER ){
        address_can = ACCELEROMETER_X_ADDR;  
        var = conv_float_to_uint(xyz[0]);
        printf("Acc addr %x: x:%d ", address_can, xyz[0]);
        emulateDevice(var, address_can, 8);

        address_can = ACCELEROMETER_Y_ADDR;
        var = conv_float_to_uint(xyz[1]);
        printf("y:%d ", xyz[1]);
        emulateDevice(var, address_can, 8);

        address_can = ACCELEROMETER_Z_ADDR;
        var = conv_float_to_uint(xyz[2]);
        printf("z:%d \n", xyz[2]);
        emulateDevice(var, address_can, 8);
      }
      else if( TEMP_SENSOR ){

        address_can = TEMP_SENSOR_ADDR;

        var = conv_float_to_uint(temperatureSensor());
        Serial.print("Temperature addr: "); Serial.print(address_can, HEX); Serial.print(" Value: "); printU64(var); Serial.println(temperature);
        emulateDevice(var, address_can, 8);
      }
    }
  } // CAN
}
